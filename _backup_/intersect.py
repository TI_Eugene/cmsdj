#!/bin/env python

old = (5, 8)

def test(b, e):
    return (e > old[0]) and (b < old[1])

print True, test(4, 7)
print True, test(4, 8)
print True, test(4, 9)
print True, test(5, 7)
print True, test(5, 8)
print True, test(5, 9)
print True, test(6, 7)
print True, test(6, 8)
print True, test(7, 9)
print False, test(3, 4)
print False, test(3, 5)
print False, test(8, 10)
print False, test(9, 10)
