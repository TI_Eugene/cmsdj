INSERT INTO enum_dow (id, name, longname) VALUES (1, 'Пн', 'Понедельник');
INSERT INTO enum_dow (id, name, longname) VALUES (2, 'Вт', 'Вторник');
INSERT INTO enum_dow (id, name, longname) VALUES (3, 'Ср', 'Среда');
INSERT INTO enum_dow (id, name, longname) VALUES (4, 'Чт', 'Четверг');
INSERT INTO enum_dow (id, name, longname) VALUES (5, 'Пт', 'Пятница');
INSERT INTO enum_dow (id, name, longname) VALUES (6, 'Сб', 'Суббота');
INSERT INTO enum_dow (id, name, longname) VALUES (7, 'Вс', 'Воскресенье');
