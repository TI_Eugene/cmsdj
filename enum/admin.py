# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *

admin.site.register(Gender)
admin.site.register(DOW)
admin.site.register(PersonAddrType)
admin.site.register(PersonPhoneType)
admin.site.register(PersonDocType)
admin.site.register(PersonCodeType)
