#!/bin/env python
# -*- coding: utf-8 -*-
'''
Script to generate peoples (person, patient, employee)
'''

import sys, os, random  # random.randint(a, b)

reload(sys)
sys.setdefaultencoding('utf-8')

EMPLOYEE_QTY    = 30
PERSON_TPL      = "INSERT INTO core_person (id, lastname, firstname, midname, gender_id, birthplace) VALUES (%d, '%s', '%s', '%s', %d, '');\n"
PATIENT_TPL     = "INSERT INTO patient_patient (id, person_id, nationality) VALUES (%d, %d, '');\n"
EMPLOYEE_TPL    = "INSERT INTO employee_employee (id, person_id) VALUES (%d, %d);\n"

def main(fn):
    # 1. load fios
    data = list()
    infile = open(fn, 'r')
    if infile:
        for l in infile:
            i, o, f = l.strip().split(' ')
            data.append((f, i, o))
        infile.close()
        # 2. generate
        isemployee = EMPLOYEE_QTY/float(len(data))
        person   = open('person.sql',   'w')
        patient  = open('patient.sql',  'w')
        employee = open('employee.sql', 'w')
        for n, (f, i, o) in enumerate(data):
            person.write(PERSON_TPL % (n+1, f, i, o, int(f.endswith('а'))))
            if (random.random() <= isemployee):
                employee.write(EMPLOYEE_TPL % (n+1, n+1))
            else:
                patient.write(PATIENT_TPL % (n+1, n+1))

if (__name__ == '__main__'):
    main(sys.argv[1])
