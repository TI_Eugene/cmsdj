function CRussianName (iFirstName, iQuaziRoot, iGender)
{
	this.mFirstName = iFirstName;
	this.mQuaziRoot = iQuaziRoot;
	this.mGender    = iGender;
}

var GENDER_MALE               = 0;
var GENDER_FEMALE             = 1;
var SECOND_NAME_FLEXIE_MALE   = "��";
var SECOND_NAME_FLEXIE_FEMALE = "��";
var LAST_NAME_FLEXIE_FEMALE   = "�";

var glbFirstNamesArray = new Array ();
glbFirstNamesArray[0] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[1] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[2] = new CRussianName ("���������", "�����������", GENDER_MALE);
glbFirstNamesArray[3] = new CRussianName ("����������", "", GENDER_FEMALE);
glbFirstNamesArray[4] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[5] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[6] = new CRussianName ("������", "�������", GENDER_MALE);
glbFirstNamesArray[7] = new CRussianName ("����", "", GENDER_FEMALE);
glbFirstNamesArray[8] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[9] = new CRussianName ("��������", "���������", GENDER_MALE);
glbFirstNamesArray[10] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[11] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[12] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[13] = new CRussianName ("��������", "����������", GENDER_MALE);
glbFirstNamesArray[14] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[15] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[16] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[17] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[18] = new CRussianName ("��������", "", GENDER_FEMALE);
glbFirstNamesArray[19] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[20] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[21] = new CRussianName ("��������", "����������", GENDER_MALE);
glbFirstNamesArray[22] = new CRussianName ("������", "�������", GENDER_MALE);
glbFirstNamesArray[23] = new CRussianName ("��������", "����������", GENDER_MALE);
glbFirstNamesArray[24] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[25] = new CRussianName ("��������", "���������", GENDER_MALE);
glbFirstNamesArray[26] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[27] = new CRussianName ("��������", "���������", GENDER_MALE);
glbFirstNamesArray[28] = new CRussianName ("������", "�������", GENDER_MALE);
glbFirstNamesArray[29] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[30] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[31] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[32] = new CRussianName ("����", "������", GENDER_MALE);
glbFirstNamesArray[33] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[34] = new CRussianName ("�����", "", GENDER_FEMALE);
glbFirstNamesArray[35] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[36] = new CRussianName ("�������", "���������", GENDER_MALE);
glbFirstNamesArray[37] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[38] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[39] = new CRussianName ("����", "������", GENDER_MALE);
glbFirstNamesArray[40] = new CRussianName ("�����", "������", GENDER_MALE);
glbFirstNamesArray[41] = new CRussianName ("����", "", GENDER_FEMALE);
glbFirstNamesArray[42] = new CRussianName ("�����", "", GENDER_FEMALE);
glbFirstNamesArray[43] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[44] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[45] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[46] = new CRussianName ("����������", "������������", GENDER_MALE);
glbFirstNamesArray[47] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[48] = new CRussianName ("���������", "�����������", GENDER_MALE);
glbFirstNamesArray[49] = new CRussianName ("����", "", GENDER_FEMALE);
glbFirstNamesArray[50] = new CRussianName ("���", "�����", GENDER_MALE);
glbFirstNamesArray[51] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[52] = new CRussianName ("�����", "", GENDER_FEMALE);
glbFirstNamesArray[53] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[54] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[55] = new CRussianName ("�����", "", GENDER_FEMALE);
glbFirstNamesArray[56] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[57] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[58] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[59] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[60] = new CRussianName ("������", "�������", GENDER_MALE);
glbFirstNamesArray[61] = new CRussianName ("�������", "���������", GENDER_MALE);
glbFirstNamesArray[62] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[63] = new CRussianName ("����", "", GENDER_FEMALE);
glbFirstNamesArray[64] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[65] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[66] = new CRussianName ("����", "������", GENDER_MALE);
glbFirstNamesArray[67] = new CRussianName ("�����", "", GENDER_FEMALE);
glbFirstNamesArray[68] = new CRussianName ("�����", "������", GENDER_MALE);
glbFirstNamesArray[69] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[70] = new CRussianName ("ϸ��", "������", GENDER_MALE);
glbFirstNamesArray[71] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[72] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[73] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[74] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[75] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[76] = new CRussianName ("��������", "", GENDER_FEMALE);
glbFirstNamesArray[77] = new CRussianName ("������", "�������", GENDER_MALE);
glbFirstNamesArray[78] = new CRussianName ("���������", "�����������", GENDER_MALE);
glbFirstNamesArray[79] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[80] = new CRussianName ("���������", "", GENDER_FEMALE);
glbFirstNamesArray[81] = new CRussianName ("������", "", GENDER_FEMALE);
glbFirstNamesArray[82] = new CRussianName ("�������", "", GENDER_FEMALE);
glbFirstNamesArray[83] = new CRussianName ("�������", "��������", GENDER_MALE);
glbFirstNamesArray[84] = new CRussianName ("������", "��������", GENDER_MALE);
glbFirstNamesArray[85] = new CRussianName ("�����", "�������", GENDER_MALE);
glbFirstNamesArray[86] = new CRussianName ("Ը���", "Ը�����", GENDER_MALE);
glbFirstNamesArray[87] = new CRussianName ("Ը���", "", GENDER_FEMALE);
glbFirstNamesArray[88] = new CRussianName ("����", "�����", GENDER_MALE);
glbFirstNamesArray[89] = new CRussianName ("��������", "", GENDER_FEMALE);
glbFirstNamesArray[90] = new CRussianName ("����", "", GENDER_FEMALE);
glbFirstNamesArray[91] = new CRussianName ("����", "�����", GENDER_MALE);
glbFirstNamesArray[92] = new CRussianName ("����", "�������", GENDER_MALE);

var glbLastNamesArray = new Array ();
glbLastNamesArray[0] = "��������";
glbLastNamesArray[1] = "�������";
glbLastNamesArray[2] = "��������";
glbLastNamesArray[3] = "������";
glbLastNamesArray[4] = "�����";
glbLastNamesArray[5] = "�������";
glbLastNamesArray[6] = "�������";
glbLastNamesArray[7] = "�������";
glbLastNamesArray[8] = "��������";
glbLastNamesArray[9] = "�����";
glbLastNamesArray[10] = "��������";
glbLastNamesArray[11] = "�����������";
glbLastNamesArray[12] = "��������";
glbLastNamesArray[13] = "������";
glbLastNamesArray[14] = "������";
glbLastNamesArray[15] = "���������";
glbLastNamesArray[16] = "��������";
glbLastNamesArray[17] = "�������";
glbLastNamesArray[18] = "��������";
glbLastNamesArray[19] = "�������";
glbLastNamesArray[20] = "���������";
glbLastNamesArray[21] = "�����������";
glbLastNamesArray[22] = "��������";
glbLastNamesArray[23] = "������";
glbLastNamesArray[24] = "�������";
glbLastNamesArray[25] = "�������";
glbLastNamesArray[26] = "������";
glbLastNamesArray[27] = "����������";
glbLastNamesArray[28] = "���������";
glbLastNamesArray[29] = "��������";
glbLastNamesArray[30] = "��������";
glbLastNamesArray[31] = "�������";
glbLastNamesArray[32] = "����������";
glbLastNamesArray[33] = "�������";
glbLastNamesArray[34] = "������";
glbLastNamesArray[35] = "�������";
glbLastNamesArray[36] = "�������";
glbLastNamesArray[37] = "��������";
glbLastNamesArray[38] = "�������";
glbLastNamesArray[39] = "������";
glbLastNamesArray[40] = "�����";
glbLastNamesArray[41] = "��������";
glbLastNamesArray[42] = "�������";
glbLastNamesArray[43] = "������";
glbLastNamesArray[44] = "�������";
glbLastNamesArray[45] = "�������";
glbLastNamesArray[46] = "�����";
glbLastNamesArray[47] = "������";
glbLastNamesArray[48] = "�������";
glbLastNamesArray[49] = "��������";
glbLastNamesArray[50] = "�����";
glbLastNamesArray[51] = "������";
glbLastNamesArray[52] = "�����";
glbLastNamesArray[53] = "�������";
glbLastNamesArray[54] = "�������";
glbLastNamesArray[55] = "��������";
glbLastNamesArray[56] = "�������";
glbLastNamesArray[57] = "�������";
glbLastNamesArray[58] = "��������";
glbLastNamesArray[59] = "���������";
glbLastNamesArray[60] = "�������";
glbLastNamesArray[61] = "������";
glbLastNamesArray[62] = "�������";
glbLastNamesArray[63] = "��������";
glbLastNamesArray[64] = "�������";
glbLastNamesArray[65] = "��������";
glbLastNamesArray[66] = "��������";
glbLastNamesArray[67] = "������";
glbLastNamesArray[68] = "��������";
glbLastNamesArray[69] = "��������";
glbLastNamesArray[70] = "��������";
glbLastNamesArray[71] = "��������";
glbLastNamesArray[72] = "��������";
glbLastNamesArray[73] = "�������";
glbLastNamesArray[74] = "���������";
glbLastNamesArray[75] = "�������";
glbLastNamesArray[76] = "��������";
glbLastNamesArray[77] = "������";
glbLastNamesArray[78] = "��������";
glbLastNamesArray[79] = "������";
glbLastNamesArray[80] = "����������";
glbLastNamesArray[81] = "�����";
glbLastNamesArray[82] = "���������";
glbLastNamesArray[83] = "������";
glbLastNamesArray[84] = "������";
glbLastNamesArray[85] = "��������";
glbLastNamesArray[86] = "����������";
glbLastNamesArray[87] = "��������";
glbLastNamesArray[88] = "�������";
glbLastNamesArray[89] = "��������";
glbLastNamesArray[90] = "���������";
glbLastNamesArray[91] = "���������";
glbLastNamesArray[92] = "��������";
glbLastNamesArray[93] = "���������";
glbLastNamesArray[94] = "������";
glbLastNamesArray[95] = "������";
glbLastNamesArray[96] = "�������";
glbLastNamesArray[97] = "�������";
glbLastNamesArray[98] = "���������";
glbLastNamesArray[99] = "��������";
glbLastNamesArray[100] = "���������";
glbLastNamesArray[101] = "������";
glbLastNamesArray[102] = "������";
glbLastNamesArray[103] = "�������";
glbLastNamesArray[104] = "��������";
glbLastNamesArray[105] = "�������";
glbLastNamesArray[106] = "�����";
glbLastNamesArray[107] = "�������";
glbLastNamesArray[108] = "�������";
glbLastNamesArray[109] = "��������";
glbLastNamesArray[110] = "�������";
glbLastNamesArray[111] = "�����";
glbLastNamesArray[112] = "���������";
glbLastNamesArray[113] = "����������";
glbLastNamesArray[114] = "��������";
glbLastNamesArray[115] = "�������";
glbLastNamesArray[116] = "�������";
glbLastNamesArray[117] = "�������";
glbLastNamesArray[118] = "�������";
glbLastNamesArray[119] = "������";
glbLastNamesArray[120] = "��������";
glbLastNamesArray[121] = "������";
glbLastNamesArray[122] = "�������";
glbLastNamesArray[123] = "�������";
glbLastNamesArray[124] = "�������";
glbLastNamesArray[125] = "�������";
glbLastNamesArray[126] = "�������";
glbLastNamesArray[127] = "��������";
glbLastNamesArray[128] = "������";
glbLastNamesArray[129] = "��������";
glbLastNamesArray[130] = "�����";
glbLastNamesArray[131] = "���������";
glbLastNamesArray[132] = "��������";
glbLastNamesArray[133] = "���������";
glbLastNamesArray[134] = "���������";
glbLastNamesArray[135] = "���������";
glbLastNamesArray[136] = "��������";
glbLastNamesArray[137] = "���������";
glbLastNamesArray[138] = "��������";
glbLastNamesArray[139] = "����������";
glbLastNamesArray[140] = "��������";
glbLastNamesArray[141] = "����������";
glbLastNamesArray[142] = "��������";
glbLastNamesArray[143] = "��������";
glbLastNamesArray[144] = "��������";
glbLastNamesArray[145] = "�������";
glbLastNamesArray[146] = "���������";
glbLastNamesArray[147] = "�����";
glbLastNamesArray[148] = "��������";
glbLastNamesArray[149] = "��������";
glbLastNamesArray[150] = "������";
glbLastNamesArray[151] = "���������";
glbLastNamesArray[152] = "���������";
glbLastNamesArray[153] = "������";
glbLastNamesArray[154] = "�������";
glbLastNamesArray[155] = "�������";
glbLastNamesArray[156] = "���������";
glbLastNamesArray[157] = "���������";
glbLastNamesArray[158] = "�������������";
glbLastNamesArray[159] = "������";
glbLastNamesArray[160] = "�������";
glbLastNamesArray[161] = "��������";
glbLastNamesArray[162] = "��������";
glbLastNamesArray[163] = "����������";
glbLastNamesArray[164] = "�����������";
glbLastNamesArray[165] = "��������";
glbLastNamesArray[166] = "�������";
glbLastNamesArray[167] = "�������";
glbLastNamesArray[168] = "�������";
glbLastNamesArray[169] = "��������";
glbLastNamesArray[170] = "�������";
glbLastNamesArray[171] = "��������";
glbLastNamesArray[172] = "��������";
glbLastNamesArray[173] = "��������";
glbLastNamesArray[174] = "��������";
glbLastNamesArray[175] = "������";
glbLastNamesArray[176] = "��������";
glbLastNamesArray[177] = "�������";
glbLastNamesArray[178] = "������";
glbLastNamesArray[179] = "������";
glbLastNamesArray[180] = "���������";
glbLastNamesArray[181] = "��������";
glbLastNamesArray[182] = "�������";
glbLastNamesArray[183] = "�������";
glbLastNamesArray[184] = "�������";
glbLastNamesArray[185] = "���������";
glbLastNamesArray[186] = "��������";
glbLastNamesArray[187] = "�������";
glbLastNamesArray[188] = "�������";
glbLastNamesArray[189] = "��������";
glbLastNamesArray[190] = "�������";
glbLastNamesArray[191] = "�������";
glbLastNamesArray[192] = "�������";
glbLastNamesArray[193] = "�������";
glbLastNamesArray[194] = "���������";
glbLastNamesArray[195] = "��������";
glbLastNamesArray[196] = "�������";
glbLastNamesArray[197] = "������";
glbLastNamesArray[198] = "�����";
glbLastNamesArray[199] = "��������";
glbLastNamesArray[200] = "�������";
glbLastNamesArray[201] = "������";
glbLastNamesArray[202] = "����������";
glbLastNamesArray[203] = "��������";
glbLastNamesArray[204] = "��������";
glbLastNamesArray[205] = "��������";
glbLastNamesArray[206] = "�������";
glbLastNamesArray[207] = "�����";
glbLastNamesArray[208] = "��������";
glbLastNamesArray[209] = "������";
glbLastNamesArray[210] = "������";
glbLastNamesArray[211] = "���������";
glbLastNamesArray[212] = "�������";
glbLastNamesArray[213] = "�������";
glbLastNamesArray[214] = "�������";
glbLastNamesArray[215] = "�������";
glbLastNamesArray[216] = "���������";
glbLastNamesArray[217] = "�������";
glbLastNamesArray[218] = "�������";
glbLastNamesArray[219] = "ĸ���";
glbLastNamesArray[220] = "������";
glbLastNamesArray[221] = "�����";
glbLastNamesArray[222] = "�������";
glbLastNamesArray[223] = "������";
glbLastNamesArray[224] = "��������";
glbLastNamesArray[225] = "���������";
glbLastNamesArray[226] = "������";
glbLastNamesArray[227] = "�������";
glbLastNamesArray[228] = "�����";
glbLastNamesArray[229] = "��������";
glbLastNamesArray[230] = "�������";
glbLastNamesArray[231] = "��������";
glbLastNamesArray[232] = "������";
glbLastNamesArray[233] = "�������";
glbLastNamesArray[234] = "��������";
glbLastNamesArray[235] = "������";
glbLastNamesArray[236] = "�����";
glbLastNamesArray[237] = "�������";
glbLastNamesArray[238] = "�������";
glbLastNamesArray[239] = "������";
glbLastNamesArray[240] = "���������";
glbLastNamesArray[241] = "�������";
glbLastNamesArray[242] = "���������";
glbLastNamesArray[243] = "�������";
glbLastNamesArray[244] = "�������";
glbLastNamesArray[245] = "�������";
glbLastNamesArray[246] = "�������";
glbLastNamesArray[247] = "������";
glbLastNamesArray[248] = "��������";
glbLastNamesArray[249] = "���������";
glbLastNamesArray[250] = "�������";
glbLastNamesArray[251] = "����";
glbLastNamesArray[252] = "������";
glbLastNamesArray[253] = "������";
glbLastNamesArray[254] = "������";
glbLastNamesArray[255] = "�������";
glbLastNamesArray[256] = "������";
glbLastNamesArray[257] = "�������";
glbLastNamesArray[258] = "�������";
glbLastNamesArray[259] = "������";
glbLastNamesArray[260] = "�����";
glbLastNamesArray[261] = "�����";
glbLastNamesArray[262] = "����������";
glbLastNamesArray[263] = "�������";
glbLastNamesArray[264] = "��������";
glbLastNamesArray[265] = "������";
glbLastNamesArray[266] = "������";
glbLastNamesArray[267] = "�������";
glbLastNamesArray[268] = "������";
glbLastNamesArray[269] = "���������";
glbLastNamesArray[270] = "�������";
glbLastNamesArray[271] = "��������";
glbLastNamesArray[272] = "����������";
glbLastNamesArray[273] = "������";
glbLastNamesArray[274] = "������";
glbLastNamesArray[275] = "�����";
glbLastNamesArray[276] = "�������";
glbLastNamesArray[277] = "�������";
glbLastNamesArray[278] = "��������";
glbLastNamesArray[279] = "�������";
glbLastNamesArray[280] = "���������";
glbLastNamesArray[281] = "���������";
glbLastNamesArray[282] = "���������";
glbLastNamesArray[283] = "������";
glbLastNamesArray[284] = "�������";
glbLastNamesArray[285] = "����������";
glbLastNamesArray[286] = "���������";
glbLastNamesArray[287] = "�������";
glbLastNamesArray[288] = "�������";
glbLastNamesArray[289] = "�������";
glbLastNamesArray[290] = "�������";
glbLastNamesArray[291] = "�������";
glbLastNamesArray[292] = "�������";
glbLastNamesArray[293] = "������";
glbLastNamesArray[294] = "�����";
glbLastNamesArray[295] = "������";
glbLastNamesArray[296] = "���������";
glbLastNamesArray[297] = "�����";
glbLastNamesArray[298] = "�������";
glbLastNamesArray[299] = "�������";
glbLastNamesArray[300] = "������";
glbLastNamesArray[301] = "������";
glbLastNamesArray[302] = "������";
glbLastNamesArray[303] = "������";
glbLastNamesArray[304] = "����������";
glbLastNamesArray[305] = "��������";
glbLastNamesArray[306] = "���������";
glbLastNamesArray[307] = "���������";
glbLastNamesArray[308] = "�����";
glbLastNamesArray[309] = "��������";
glbLastNamesArray[310] = "�������";
glbLastNamesArray[311] = "��������";
glbLastNamesArray[312] = "�������";
glbLastNamesArray[313] = "��������";
glbLastNamesArray[314] = "��������";
glbLastNamesArray[315] = "�������";
glbLastNamesArray[316] = "�������";
glbLastNamesArray[317] = "�������";
glbLastNamesArray[318] = "������";
glbLastNamesArray[319] = "�������";
glbLastNamesArray[320] = "��������";
glbLastNamesArray[321] = "���������";
glbLastNamesArray[322] = "����������";
glbLastNamesArray[323] = "��������";
glbLastNamesArray[324] = "�������";
glbLastNamesArray[325] = "�������";
glbLastNamesArray[326] = "�������";
glbLastNamesArray[327] = "���������";
glbLastNamesArray[328] = "���������";
glbLastNamesArray[329] = "��������";
glbLastNamesArray[330] = "���������";
glbLastNamesArray[331] = "���������";
glbLastNamesArray[332] = "���������";
glbLastNamesArray[333] = "�������";
glbLastNamesArray[334] = "��������";
glbLastNamesArray[335] = "��������";
glbLastNamesArray[336] = "���������";
glbLastNamesArray[337] = "��������";
glbLastNamesArray[338] = "��������";
glbLastNamesArray[339] = "���������";
glbLastNamesArray[340] = "��������";
glbLastNamesArray[341] = "������";
glbLastNamesArray[342] = "��������";
glbLastNamesArray[343] = "�������";
glbLastNamesArray[344] = "��������";
glbLastNamesArray[345] = "������";
glbLastNamesArray[346] = "�����";
glbLastNamesArray[347] = "���������";
glbLastNamesArray[348] = "�������";
glbLastNamesArray[349] = "���������";
glbLastNamesArray[350] = "����������";
glbLastNamesArray[351] = "�������";
glbLastNamesArray[352] = "�������";
glbLastNamesArray[353] = "������";
glbLastNamesArray[354] = "�������";
glbLastNamesArray[355] = "�������";
glbLastNamesArray[356] = "�������";
glbLastNamesArray[357] = "�����������";
glbLastNamesArray[358] = "������";
glbLastNamesArray[359] = "������";
glbLastNamesArray[360] = "��������";
glbLastNamesArray[361] = "�������";
glbLastNamesArray[362] = "���������";
glbLastNamesArray[363] = "������";
glbLastNamesArray[364] = "������";
glbLastNamesArray[365] = "�������";
glbLastNamesArray[366] = "�������";
glbLastNamesArray[367] = "������";
glbLastNamesArray[368] = "�����";
glbLastNamesArray[369] = "������";
glbLastNamesArray[370] = "�����";
glbLastNamesArray[371] = "��������";
glbLastNamesArray[372] = "���������";
glbLastNamesArray[373] = "�������";
glbLastNamesArray[374] = "�������";
glbLastNamesArray[375] = "�������";
glbLastNamesArray[376] = "�������";
glbLastNamesArray[377] = "��������";
glbLastNamesArray[378] = "�������";
glbLastNamesArray[379] = "��������";
glbLastNamesArray[380] = "�������";
glbLastNamesArray[381] = "��������";
glbLastNamesArray[382] = "��������";
glbLastNamesArray[383] = "������";
glbLastNamesArray[384] = "�������";
glbLastNamesArray[385] = "�������";
glbLastNamesArray[386] = "������";
glbLastNamesArray[387] = "��������";
glbLastNamesArray[388] = "�������";
glbLastNamesArray[389] = "�����";
glbLastNamesArray[390] = "������";
glbLastNamesArray[391] = "�������";
glbLastNamesArray[392] = "�������";
glbLastNamesArray[393] = "˸������";
glbLastNamesArray[394] = "�������";
glbLastNamesArray[395] = "�������";
glbLastNamesArray[396] = "�����";
glbLastNamesArray[397] = "���������";
glbLastNamesArray[398] = "�������";
glbLastNamesArray[399] = "�������";
glbLastNamesArray[400] = "�������";
glbLastNamesArray[401] = "�������";
glbLastNamesArray[402] = "��������";
glbLastNamesArray[403] = "�����";
glbLastNamesArray[404] = "��������";
glbLastNamesArray[405] = "������";
glbLastNamesArray[406] = "���������";
glbLastNamesArray[407] = "������";
glbLastNamesArray[408] = "�����";
glbLastNamesArray[409] = "�������";
glbLastNamesArray[410] = "�������";
glbLastNamesArray[411] = "��������";
glbLastNamesArray[412] = "����������";
glbLastNamesArray[413] = "������";
glbLastNamesArray[414] = "������";
glbLastNamesArray[415] = "�������";
glbLastNamesArray[416] = "������";
glbLastNamesArray[417] = "�������";
glbLastNamesArray[418] = "��������";
glbLastNamesArray[419] = "�����";
glbLastNamesArray[420] = "�����";
glbLastNamesArray[421] = "�����";
glbLastNamesArray[422] = "�����";
glbLastNamesArray[423] = "�������";
glbLastNamesArray[424] = "�����";
glbLastNamesArray[425] = "�������";
glbLastNamesArray[426] = "�����";
glbLastNamesArray[427] = "������";
glbLastNamesArray[428] = "�����";
glbLastNamesArray[429] = "�������";
glbLastNamesArray[430] = "��������";
glbLastNamesArray[431] = "������";
glbLastNamesArray[432] = "�������";
glbLastNamesArray[433] = "�������";
glbLastNamesArray[434] = "��������";
glbLastNamesArray[435] = "��������";
glbLastNamesArray[436] = "��������";
glbLastNamesArray[437] = "��������";
glbLastNamesArray[438] = "��������";
glbLastNamesArray[439] = "�������";
glbLastNamesArray[440] = "��������";
glbLastNamesArray[441] = "��������";
glbLastNamesArray[442] = "��������";
glbLastNamesArray[443] = "�������";
glbLastNamesArray[444] = "������";
glbLastNamesArray[445] = "��������";
glbLastNamesArray[446] = "���������";
glbLastNamesArray[447] = "��������";
glbLastNamesArray[448] = "���������";
glbLastNamesArray[449] = "�������";
glbLastNamesArray[450] = "���������";
glbLastNamesArray[451] = "��������";
glbLastNamesArray[452] = "���������";
glbLastNamesArray[453] = "���������";
glbLastNamesArray[454] = "�������";
glbLastNamesArray[455] = "���������";
glbLastNamesArray[456] = "�������";
glbLastNamesArray[457] = "������";
glbLastNamesArray[458] = "�������";
glbLastNamesArray[459] = "�������";
glbLastNamesArray[460] = "������";
glbLastNamesArray[461] = "�����";
glbLastNamesArray[462] = "�������";
glbLastNamesArray[463] = "��������";
glbLastNamesArray[464] = "����������";
glbLastNamesArray[465] = "��������";
glbLastNamesArray[466] = "���������";
glbLastNamesArray[467] = "�������";
glbLastNamesArray[468] = "������";
glbLastNamesArray[469] = "��������";
glbLastNamesArray[470] = "��������";
glbLastNamesArray[471] = "�������";
glbLastNamesArray[472] = "�������";
glbLastNamesArray[473] = "���������";
glbLastNamesArray[474] = "��������";
glbLastNamesArray[475] = "��������";
glbLastNamesArray[476] = "���������";
glbLastNamesArray[477] = "��������";
glbLastNamesArray[478] = "���������";
glbLastNamesArray[479] = "������";
glbLastNamesArray[480] = "�������";
glbLastNamesArray[481] = "�������";
glbLastNamesArray[482] = "�������";
glbLastNamesArray[483] = "�������";
glbLastNamesArray[484] = "�������";
glbLastNamesArray[485] = "�������";
glbLastNamesArray[486] = "��������";
glbLastNamesArray[487] = "�����";
glbLastNamesArray[488] = "������";
glbLastNamesArray[489] = "�������";
glbLastNamesArray[490] = "�������";
glbLastNamesArray[491] = "�������";
glbLastNamesArray[492] = "���������";
glbLastNamesArray[493] = "�������";
glbLastNamesArray[494] = "�������";
glbLastNamesArray[495] = "�������";
glbLastNamesArray[496] = "��������";
glbLastNamesArray[497] = "����������";
glbLastNamesArray[498] = "�������";
glbLastNamesArray[499] = "���������";
glbLastNamesArray[500] = "��������";
glbLastNamesArray[501] = "���������";
glbLastNamesArray[502] = "�������";
glbLastNamesArray[503] = "�������";
glbLastNamesArray[504] = "����������";
glbLastNamesArray[505] = "�������";
glbLastNamesArray[506] = "�������";
glbLastNamesArray[507] = "���������";
glbLastNamesArray[508] = "���������";
glbLastNamesArray[509] = "�������";
glbLastNamesArray[510] = "��������";
glbLastNamesArray[511] = "�������";
glbLastNamesArray[512] = "�����";
glbLastNamesArray[513] = "�������";
glbLastNamesArray[514] = "�������";
glbLastNamesArray[515] = "�������";
glbLastNamesArray[516] = "��������";
glbLastNamesArray[517] = "��������";
glbLastNamesArray[518] = "��������";
glbLastNamesArray[519] = "��������";
glbLastNamesArray[520] = "��������";
glbLastNamesArray[521] = "������";
glbLastNamesArray[522] = "�����";
glbLastNamesArray[523] = "�����";
glbLastNamesArray[524] = "��������";
glbLastNamesArray[525] = "������";
glbLastNamesArray[526] = "��������";
glbLastNamesArray[527] = "�������";
glbLastNamesArray[528] = "���������";
glbLastNamesArray[529] = "���������";
glbLastNamesArray[530] = "�������";
glbLastNamesArray[531] = "������";
glbLastNamesArray[532] = "�������";
glbLastNamesArray[533] = "�������";
glbLastNamesArray[534] = "�������";
glbLastNamesArray[535] = "��������";
glbLastNamesArray[536] = "����������";
glbLastNamesArray[537] = "��������";
glbLastNamesArray[538] = "��������";
glbLastNamesArray[539] = "����������";
glbLastNamesArray[540] = "��������";
glbLastNamesArray[541] = "�������";
glbLastNamesArray[542] = "��������";
glbLastNamesArray[543] = "��������";
glbLastNamesArray[544] = "�������";
glbLastNamesArray[545] = "���������";
glbLastNamesArray[546] = "��������";
glbLastNamesArray[547] = "�������";
glbLastNamesArray[548] = "�������";
glbLastNamesArray[549] = "���������";
glbLastNamesArray[550] = "��������";
glbLastNamesArray[551] = "���������";
glbLastNamesArray[552] = "���������";
glbLastNamesArray[553] = "����������";
glbLastNamesArray[554] = "���������";
glbLastNamesArray[555] = "��������";
glbLastNamesArray[556] = "���������";
glbLastNamesArray[557] = "���������";
glbLastNamesArray[558] = "���������";
glbLastNamesArray[559] = "����������";
glbLastNamesArray[560] = "����������";
glbLastNamesArray[561] = "����������";
glbLastNamesArray[562] = "���������";
glbLastNamesArray[563] = "����������";
glbLastNamesArray[564] = "���������";
glbLastNamesArray[565] = "����������";
glbLastNamesArray[566] = "����������";
glbLastNamesArray[567] = "���������";
glbLastNamesArray[568] = "����������";
glbLastNamesArray[569] = "�����������";
glbLastNamesArray[570] = "��������";
glbLastNamesArray[571] = "����������";
glbLastNamesArray[572] = "���������";
glbLastNamesArray[573] = "����������";
glbLastNamesArray[574] = "����������";
glbLastNamesArray[575] = "���������";
glbLastNamesArray[576] = "����������";
glbLastNamesArray[577] = "���������";
glbLastNamesArray[578] = "���������";
glbLastNamesArray[579] = "�����";
glbLastNamesArray[580] = "��������";
glbLastNamesArray[581] = "���������";
glbLastNamesArray[582] = "��������";
glbLastNamesArray[583] = "�������";
glbLastNamesArray[584] = "��������";
glbLastNamesArray[585] = "��������";
glbLastNamesArray[586] = "����������";
glbLastNamesArray[587] = "������";
glbLastNamesArray[588] = "�������";
glbLastNamesArray[589] = "��������";
glbLastNamesArray[590] = "��������";
glbLastNamesArray[591] = "�������";
glbLastNamesArray[592] = "����������";
glbLastNamesArray[593] = "������";
glbLastNamesArray[594] = "������";
glbLastNamesArray[595] = "������������";
glbLastNamesArray[596] = "�������";
glbLastNamesArray[597] = "���������";
glbLastNamesArray[598] = "���������";
glbLastNamesArray[599] = "���������";
glbLastNamesArray[600] = "�������";
glbLastNamesArray[601] = "�����������";
glbLastNamesArray[602] = "���������";
glbLastNamesArray[603] = "����������";
glbLastNamesArray[604] = "�����������";
glbLastNamesArray[605] = "������";
glbLastNamesArray[606] = "���������";
glbLastNamesArray[607] = "������";
glbLastNamesArray[608] = "������";
glbLastNamesArray[609] = "�������";
glbLastNamesArray[610] = "�����������";
glbLastNamesArray[611] = "���������";
glbLastNamesArray[612] = "����������";
glbLastNamesArray[613] = "���������";
glbLastNamesArray[614] = "������";
glbLastNamesArray[615] = "�������";
glbLastNamesArray[616] = "��������";
glbLastNamesArray[617] = "�������";
glbLastNamesArray[618] = "�������";
glbLastNamesArray[619] = "�������";
glbLastNamesArray[620] = "�������";
glbLastNamesArray[621] = "��������";
glbLastNamesArray[622] = "������������";
glbLastNamesArray[623] = "������";
glbLastNamesArray[624] = "��������";
glbLastNamesArray[625] = "��������";
glbLastNamesArray[626] = "���������";
glbLastNamesArray[627] = "��������";
glbLastNamesArray[628] = "����������";
glbLastNamesArray[629] = "��������";
glbLastNamesArray[630] = "��������";
glbLastNamesArray[631] = "�������";
glbLastNamesArray[632] = "�������";
glbLastNamesArray[633] = "���������";
glbLastNamesArray[634] = "�����";
glbLastNamesArray[635] = "�������";
glbLastNamesArray[636] = "������";
glbLastNamesArray[637] = "��������";
glbLastNamesArray[638] = "����������";
glbLastNamesArray[639] = "�������";
glbLastNamesArray[640] = "���������";
glbLastNamesArray[641] = "���������";
glbLastNamesArray[642] = "�����";
glbLastNamesArray[643] = "��������";
glbLastNamesArray[644] = "��������";
glbLastNamesArray[645] = "������";
glbLastNamesArray[646] = "�������";
glbLastNamesArray[647] = "�����";
glbLastNamesArray[648] = "�������";
glbLastNamesArray[649] = "��������";
glbLastNamesArray[650] = "�������";
glbLastNamesArray[651] = "�����";
glbLastNamesArray[652] = "������";
glbLastNamesArray[653] = "�������";
glbLastNamesArray[654] = "�������";
glbLastNamesArray[655] = "���������";
glbLastNamesArray[656] = "��������";
glbLastNamesArray[657] = "��������";
glbLastNamesArray[658] = "������";
glbLastNamesArray[659] = "�������";
glbLastNamesArray[660] = "����������";
glbLastNamesArray[661] = "�������";
glbLastNamesArray[662] = "��������";
glbLastNamesArray[663] = "�������";
glbLastNamesArray[664] = "�������";
glbLastNamesArray[665] = "�������";
glbLastNamesArray[666] = "��������";
glbLastNamesArray[667] = "���������";
glbLastNamesArray[668] = "����������";
glbLastNamesArray[669] = "��������";
glbLastNamesArray[670] = "��������";
glbLastNamesArray[671] = "������";
glbLastNamesArray[672] = "�������";
glbLastNamesArray[673] = "��������";
glbLastNamesArray[674] = "���������";
glbLastNamesArray[675] = "���������";
glbLastNamesArray[676] = "��������";
glbLastNamesArray[677] = "��������";
glbLastNamesArray[678] = "���������";
glbLastNamesArray[679] = "��������";
glbLastNamesArray[680] = "������";
glbLastNamesArray[681] = "�������";
glbLastNamesArray[682] = "������";
glbLastNamesArray[683] = "������";
glbLastNamesArray[684] = "������";
glbLastNamesArray[685] = "��������";
glbLastNamesArray[686] = "�����";
glbLastNamesArray[687] = "��������";
glbLastNamesArray[688] = "������";
glbLastNamesArray[689] = "�����������";
glbLastNamesArray[690] = "��������";
glbLastNamesArray[691] = "����������";
glbLastNamesArray[692] = "�������";
glbLastNamesArray[693] = "�������";
glbLastNamesArray[694] = "�����";
glbLastNamesArray[695] = "�����";
glbLastNamesArray[696] = "��������";
glbLastNamesArray[697] = "�����";
glbLastNamesArray[698] = "�������";
glbLastNamesArray[699] = "������";
glbLastNamesArray[700] = "��������";
glbLastNamesArray[701] = "�������";
glbLastNamesArray[702] = "�������";
glbLastNamesArray[703] = "�������";
glbLastNamesArray[704] = "������";
glbLastNamesArray[705] = "�������";
glbLastNamesArray[706] = "��������";
glbLastNamesArray[707] = "��������";
glbLastNamesArray[708] = "����";
glbLastNamesArray[709] = "������";

function randomNumber (iUpperBound)
{
	return Math.round (Math.random () * (iUpperBound - 1));
}

function getRandomRussianFirstName (iGender)
{
	var lclFirstName = null;

	do
	{
		lclFirstName = glbFirstNamesArray[randomNumber (glbFirstNamesArray.length)];
	}
	while (lclFirstName.mGender != iGender);

	return lclFirstName;
}

function getRandomRussianLastName ()
{
	return glbLastNamesArray[randomNumber (glbLastNamesArray.length)];
}

function getRandomRussianName ()
{
	var lclGender           = GENDER_MALE;
	var lclSecondNameFlexie = SECOND_NAME_FLEXIE_MALE;
	var lclLastNameFlexie   = "";

	if (Math.random () < 0.5)
	{
		lclGender = GENDER_FEMALE;
	}
	var lclFirstName = getRandomRussianFirstName (lclGender);
	var lclSecondName = getRandomRussianFirstName (GENDER_MALE);
	var lclLastName = getRandomRussianLastName ();

	if (lclGender == GENDER_FEMALE)
	{
		lclSecondNameFlexie = SECOND_NAME_FLEXIE_FEMALE;
		lclLastNameFlexie   = LAST_NAME_FLEXIE_FEMALE;
	}
	else if (lclSecondName.mQuaziRoot.substr (lclSecondName.mQuaziRoot.length - 2, 2) == SECOND_NAME_FLEXIE_MALE)
	{
		lclSecondNameFlexie = "";
	}

	return lclFirstName.mFirstName + " " + lclSecondName.mQuaziRoot + lclSecondNameFlexie + " " + lclLastName + lclLastNameFlexie;
}
