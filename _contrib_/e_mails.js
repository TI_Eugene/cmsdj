var atSign = "@";
var underlineSign = "_";

var mailSPArray = new Array ();
mailSPArray[0] = "yandex.ru";
mailSPArray[1] = "mail.ru";
mailSPArray[2] = "inbox.ru";
mailSPArray[3] = "hotbox.ru";
mailSPArray[4] = "smtp.ru";
mailSPArray[5] = "pop3.ru";
mailSPArray[6] = "chat.ru";
mailSPArray[7] = "list.ru";
mailSPArray[8] = "rambler.ru";
mailSPArray[9] = "rin.ru";
mailSPArray[10] = "mnogo.ru";
mailSPArray[11] = "orc.ru";
mailSPArray[12] = "newmail.ru";
mailSPArray[13] = "max.ru";
mailSPArray[14] = "netman.ru";
mailSPArray[15] = "email.ru";
mailSPArray[16] = "land.ru";
mailSPArray[17] = "narod.ru";
mailSPArray[18] = "rumail.ru";
mailSPArray[19] = "exn.ru";
mailSPArray[20] = "umr.ru";
mailSPArray[21] = "yahoo.com";
mailSPArray[22] = "usa.net";
mailSPArray[23] = "netaddress.com";
mailSPArray[24] = "visto.com";
mailSPArray[25] = "nettaxi.com";
mailSPArray[26] = "softhome.com";
mailSPArray[27] = "langoo.com";
mailSPArray[28] = "rol.ru";
mailSPArray[29] = "aol.com";
mailSPArray[30] = "sexnarod.ru";
mailSPArray[31] = "infobiz.com";
mailSPArray[32] = "adelite.com";
mailSPArray[33] = "hob.ru";
mailSPArray[34] = "xaker.ru";
mailSPArray[35] = "nextmail.ru";
mailSPArray[36] = "netbox.com";
mailSPArray[37] = "hotbox.com";
mailSPArray[38] = "lycos.com";
mailSPArray[39] = "google.com";
mailSPArray[40] = "nihuja.net";

var consonantArray = new Array ("b", "c", "ch", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "qu", "r", "s", "sch", "sh", "t", "th", "v", "x", "z", "zh");
var vowelArray     = new Array ("a", "ae", "e", "ee", "i", "ia", "ie", "io", "iu", "ja", "je", "ji", "jo", "ju", "o", "oo", "u", "ou", "y", "ya", "ye", "yi", "yo", "yu");

function randomNumber (iUpperBound)
{
	return Math.round (Math.random () * (iUpperBound - 1));
}

function getRandomEMail ()
{
	var name = "";
	var domain = atSign + mailSPArray[randomNumber (mailSPArray.length)];

	var nofLetters = 3 + randomNumber (5);
	for (lclIndex = 0; lclIndex < nofLetters; lclIndex++)
	{
		if (lclIndex % 2 == 0)
		{
			name += consonantArray[randomNumber (consonantArray.length)];
		}
		else
		{
			name += vowelArray[randomNumber (vowelArray.length)];
		}
	}
	
	if (name.length < 7)
	{
		if (Math.random () < 0.1)
		{
			name += underlineSign + (1980 + randomNumber (25));
		}
	}
	
	return (name + domain);
}