import sys, os
PROJECT_DIR = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (('Mr. Odmin', 'odmin@example.com'),
)

MANAGERS = ADMINS

DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3', 'NAME': 'cmsdj.db',}}

TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-RU'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media'),
MEDIA_URL = ''
STATIC_ROOT = ''
STATIC_URL = '/static_cmsdj/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'static'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'dajaxice.finders.DajaxiceFinder',  # dajaxice
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)
#SECRET_KEY = 'bf(+*va(yj&amp;#498fv_8m378*ff$+qaf*vjsb2c^bj3!43aj^=c'
SECRET_KEY = '4(&d8btp)r+tv23+xp!aa_@zz30mau9nz_2+1hb3-9u)jo5^35'
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',  # dajaxice
)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)
ROOT_URLCONF = 'urls'
WSGI_APPLICATION = 'wsgi.application'
TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)
JINJA2_TEMPLATE_DIRS = TEMPLATE_DIRS
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    #'dajaxice', # dajaxice
    #'dajax',    # dajax
    'utils',
    'core',
    'enum',
    'ref',
    'employee',
    'patient',
)

CREATE_DEFAULT_SUPERUSER = True

try:
        from local_settings import *
except ImportError:
        pass
